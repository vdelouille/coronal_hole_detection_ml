function  plotROC(filename,num_fig)

% Function to plot in figure(num_fig) the ROC curve for results present in filename
%
% Possible values for 'filename':
% 
% 'resultsStr_empirical_aia_hmi_shape_mag_solarXY_cor.mat'
% 'resultsStr_uniform_aia_hmi_shape_mag_solarXY_cor.mat'
% 'resultsStr_ADASYN_empirical_aia_hmi_shape_mag_solarXY_cor.mat'
% 'resultsStr_ADASYN_uniform_aia_hmi_shape_mag_solarXY_cor.mat'
% 'resultsStr_ensemble_empirical_aia_hmi_shape_mag_solarXY_cor.mat'
% 'resultsStr_ensemble_uniform_aia_hmi_shape_mag_solarXY_cor.mat'
% 'resultsStr_uniform_aia_hmi_magXY_cor.mat'
% 'resultsStr_uniform_aia_shape_solarXY.mat'
% 'resultsStr_uniform_aia_solarXY.mat'
%
%  %%% Set default values of num_fig
if nargin<2     num_fig = 1;     end
%
[pathstr,nom_base,ext] = fileparts(filename) ;
load(filename)

key = nom_base(12:19);

nlearners=size(TSS,2);

% Use Score and true labels from each Learner
% to compute FPR and TPR with Matlab function perfcurve.m 
% fpr is false positive rate
% tpr is true positive rate
% AUC is Area Under the Curve
% optrocpt is Optimal operating point of the ROC curve as computed by Matlab

AUC=zeros(3,nlearners);
optrocpt = zeros(nlearners,2);

if strcmp(key,'ensemble')
    YBoost = Ytrue(:,1);
    ScoreBoost = Score(:,1);
    [fprBoost,tprBoost,~,AUC(:,1)] = perfcurve(YBoost,ScoreBoost,1);
    YRUS = Ytrue(:,2);
    ScoreRUS = Score(:,2);
    [fprRUS,tprRUS,~,AUC(:,2)] = perfcurve(YRUS,ScoreRUS,1);
else

    YSVMLin = Ytrue(:,1);
    ScoreSVMLin = Score(:,1);
    [fprSVMLin,tprSVMLin,~,AUC(:,1),optrocpt(1,:)] = perfcurve(YSVMLin,ScoreSVMLin,1);

    YSVM = Ytrue(:,2);
    ScoreSVM = Score(:,2);
    [fprSVM,tprSVM,~,AUC(:,2),optrocpt(2,:)] = perfcurve(YSVM,ScoreSVM,1);

    YTree = Ytrue(:,3);
    ScoreTree = Score(:,3);

    [fprTree,tprTree,~,AUC(:,3),optrocpt(3,:)] = perfcurve(YTree,ScoreTree,1);

    YRandomForest = Ytrue(:,4);
    ScoreRandomForest =Score(:,4);
    [fprRF,tprRF,~,AUC(:,4),optrocpt(4,:)] = perfcurve(YRandomForest,ScoreRandomForest ,1);

    YKNN= Ytrue(:,5);
    ScoreKNN = Score(:,5);
    [fprKNN ,tprKNN ,~,AUC(:,5),optrocpt(5,:)] = perfcurve(YKNN,ScoreKNN,1);
end

close all % close all figures


if strcmp(key,'ensemble')
    figure(num_fig)
      ind=find(tprBoost>=0.90);
    ind1=ind(1);
    % ROC Curve for ADABoost.M1
    plot(fprBoost(:,1),tprBoost(:,1),'r-','DisplayName',strcat('ADABoost - AUC: ',num2str(AUC(1,1),2)),'Linewidth',1.5)
    hold on
    plot(fprBoost(ind1,1),tprBoost(ind1,1),'ro','DisplayName',strcat('[',num2str(fprBoost(ind1,1),2),',',num2str(tprBoost(ind1,1),2),']'))
    
    % ROC curve for RUSBoost
    plot(fprRUS(:,1),tprRUS(:,1),'b-','DisplayName',strcat('RUSBoost - AUC: ',num2str(AUC(1,2),2)),'Linewidth',1.5)

    legend('show','Location','southeast')
    xlabel('FPR');ylabel('TPR')
    ylim([0,1.001])
    xlim([-0.01,1.001])
    ax = gca; % current axes
    ax.FontSize = 12;
    ax.TickDir = 'out';
    ax.TickLength = [0.02 0.02];

    name_plot=strcat('ROC_',nom_base)
    print(name_plot,'-dpng')
    hold off
else
    ind=find(tprSVMLin>=0.90);
    ind1=ind(1);
    %nd = find(tprSVMLin>=0.80);
    %ind2 =ind(1);
    figure(num_fig)
    % ROC Curve for Linear SVM with mean AUC indicated in the legend
    plot(fprSVMLin(:,1),tprSVMLin(:,1),'r-','DisplayName',strcat('Lin SVM - AUC: ',num2str(AUC(1,1),2)),'Linewidth',1.5)
    hold on
    plot(fprSVMLin(ind1,1),tprSVMLin(ind1,1),'ro','DisplayName',strcat('[',num2str(fprSVMLin(ind1,1),2),',',num2str(tprSVMLin(ind1,1),2),']'))
    %plot(fprSVMLin(ind2,1),tprSVMLin(ind2,1),'ko','DisplayName',strcat('[',num2str(fprSVMLin(ind2,1),2),',',num2str(tprSVMLin(ind2,1),2),']'))
    
    
    % ROC curve for SVM 
    plot(fprSVM(:,1),tprSVM(:,1),'b-','DisplayName',strcat('SVM - AUC: ',num2str(AUC(1,2),2)),'Linewidth',1.5)

    % ROC curve for Decision Tree
    plot(fprTree(:,1),tprTree(:,1),'g-','DisplayName',strcat('Tree - AUC: ',num2str(AUC(1,3),2)),'Linewidth',1.5)

    % ROC curve for Random Forest
    plot(fprRF(:,1),tprRF(:,1),'m-','DisplayName',strcat('RF - AUC: ',num2str(AUC(1,4),2)),'Linewidth',1.5)

    % ROC curve for KNN
    plot(fprKNN(:,1),tprKNN(:,1),'k-','DisplayName',strcat('k-NN - AUC: ',num2str(AUC(1,5),2)),'Linewidth',1.5)

    legend('show','Location','southeast')
    xlabel('FPR');ylabel('TPR')
    ylim([0,1.001])
    xlim([-0.01,1.001])

    ax = gca; % current axes
    ax.FontSize = 12;
    ax.TickDir = 'out';
    ax.TickLength = [0.02 0.02];

    name_plot=strcat('ROC_',nom_base)
    print(name_plot,'-dpng')
    hold off   
end