function [X,y,header] = import_aia(filename, startRow, endRow)
%
%--------------------------------------------------------------------------------------------
% IMPORTFILE Import numeric data from a text file as column vectors.
%   [X,y,header] = IMPORT_AIA(FILENAME) Reads data from text file FILENAME for the
%   default selection.
%               
%   [X,y,header]  = IMPORT_AIA(FILENAME, STARTROW, ENDROW) 
%     Reads data from rows STARTROW through ENDROW of text file FILENAME.
%
%   Here FILENAME must be 'aia_solarXY.csv' and outputs are:
%               X: a (nxp) matrix of feature vectors computed from AIA images,
%                    where n is number of observations, p is number of features
%               y: vectors of labels (nx1) corresponding to features in X. 
%                   y=1 when features is a Filament Channel and y=0 for coronal holes)
%               header: cell array of size px1, cell(i,1) contains the name of feature i
%
% Example:
%   [X,y,header] = import_aia('aia_solarXY.csv',2, 5004);
%
% % X, y, header are then saved into 'aia_solarXY.mat'
% ----------------------------------------------------------------------------------------------
%% Initialize variables.
delimiter = ',';
if nargin<=2
    startRow = 2;
    endRow = inf;
end

%% Format for each line of text:
%   column1: double (%f)
%	column2: double (%f)
%   column3: double (%f)
%	column4: double (%f)
%   column5: double (%f)
%	column6: double (%f)
%   column7: double (%f)
%	column8: double (%f)
%   column9: double (%f)
%	column10: double (%f)
%   column11: double (%f)
%	column12: double (%f)
%   column13: double (%f)
%	column14: double (%f)
%   column15: double (%f)
%	column16: double (%f)
%   column17: double (%f)
%	column18: double (%f)
%   column19: double (%f)
%	column20: double (%f)
%   column21: double (%f)
%	column22: double (%f)
%   column23: double (%f)
%	column24: double (%f)
%   column25: double (%f)
%	column26: double (%f)
%   column27: double (%f)
%	column28: double (%f)
%   column29: double (%f)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', startRow(1)-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
for block=2:length(startRow)
    frewind(fileID);
    dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', startRow(block)-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
    for col=1:length(dataArray)
        dataArray{col} = [dataArray{col};dataArrayBlock{col}];
    end
end

%% Close the text file.
fclose(fileID);

%% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

%% Allocate imported array to column variable names
absMeanAIA = dataArray{:, 1};
Contrast1AIA = dataArray{:, 2};
VarianceAIA = dataArray{:, 3};
Entropy1AIA = dataArray{:, 4};
Energy1AIA = dataArray{:, 5};
Energy2AIA = dataArray{:, 6};
Contrast2AIA = dataArray{:, 7};
CorrelationAIA = dataArray{:, 8};
SqVarianceAIA = dataArray{:, 9};
HomogeneityAIA = dataArray{:, 10};
SumAverageAIA = dataArray{:, 11};
SumVarianceAIA = dataArray{:, 12};
SumEntropyAIA = dataArray{:, 13};
EntropyAIA = dataArray{:, 14};
DiffVarianceAIA = dataArray{:, 15};
DiffEntropyAIA = dataArray{:, 16};
Info1AIA = dataArray{:, 17};
Info2AIA = dataArray{:, 18};
meanXAIA = dataArray{:, 19};
StdDevXAIA = dataArray{:, 20};
HXY1AIA = dataArray{:, 21};
HXY2AIA = dataArray{:, 22};
Areakm2 = dataArray{:, 23};
Areaarcsec2 = dataArray{:, 24};
Distancetodiskcenterarcsec = dataArray{:, 25};
minlat = dataArray{:, 26};
maxlat = dataArray{:, 27};
CofMlat = dataArray{:, 28};
CHFIL01 = dataArray{:, 29};

% Place feature vectors in a matrix X
% We leave out Areaarcsec2
    X=cat(2,absMeanAIA,Contrast1AIA,VarianceAIA,Entropy1AIA,Energy1AIA,Energy2AIA,Contrast2AIA,CorrelationAIA,SqVarianceAIA,HomogeneityAIA,...
    SumAverageAIA,SumVarianceAIA,SumEntropyAIA,EntropyAIA,DiffVarianceAIA,DiffEntropyAIA,Info1AIA,Info2AIA,meanXAIA,StdDevXAIA,HXY1AIA,HXY2AIA,...
    Areakm2,Distancetodiskcenterarcsec,minlat,maxlat,CofMlat);


% The labels'CH' and 'FL'  are contained in CHFIL01. '1' is for FL, '0' is for CH
y=CHFIL01;

% keep header for variable names:


header=cell(27,1);
header{1}=string('absMeanAIA');
header{2}=string('Contrast1AIA');
header{3}=string('VarianceAIA');
header{4}=string('Entropy1AIA');
header{5}=string('Energy1AIA');
header{6}=string('Energy2AIA');
header{7}=string('Contrast2AIA');
header{8}=string('CorrelationAIA');
header{9}=string('SqVarianceAIA');
header{10}=string('HomogeneityAIA');
header{11}=string('SumAverageAIA');
header{12}=string('SumVarianceAIA');
header{13}=string('SumEntropyAIA');
header{14}=string('EntropyAIA');
header{15}=string('DiffVarianceAIA');
header{16}=string('DiffEntropyAIA');
header{17}=string('Info1AIA');
header{18}=string('Info2AIA');
header{19}=string('meanXAIA');
header{20}=string('StdDevXAIA');
header{21}=string('HXY1AIA');
header{22}=string('HXY2AIA');
header{23}=string('Areakm2');
header{24}=string('Distancetodiskcenterarcsec');
header{25}=string('minlat'); 
header{26}=string('maxlat');
header{27}=string('CofMlat');



