#   Coronal holes detection using supervised classification  coronal_hole_detection_ml

 * Object: We demonstrate the use of machine learning algorithms in combination with segmentation techniques in order to distinguish coronal holes and filaments in solar EUV images recorded by SDO/AIA.

 * Content: This folder contains data, matlab programs, and result files the Chapter 'Coronal holes detection using supervised classification' from the book on 'Machine Learning for Space Weather', Elsevier,  2017.
 * Matlab Version 2016b and the Statistics and Machine Learning toolbox are used.
 * Corresponding author : V. Delouille 

## Instructions:
 Add the folder 'coronal_hole_detection_ml' to your matlab path:
    >>addpath(genpath(strcat(yourpath,'coronal_hole_detection_ml/')))
where 'yourpath' is your working directory.
 
## Folder contents 
 * Main folder contains the matlab programs
 'meta_pipeline.m' is the main function, that calls all the other functions.
 In 'meta_pipeline.m' you need to specify where to find the dataset to be analysed
    >>pathdata=strcat(yourpath,'coronal_hole_detection_ml/data/')

 * Folder ./data/ contains the labeled dataset used for this study:
  FeatureValues_solarXY.csv contains all attributes, also the date from which the feature was observed (in filename). The second row contains brief explanation about the attributes.
From FeaturesValues_solarXY.csv we derived  the four sets of attributes:

 1. aia_solarXY.mat and aia_solarXY.csv are contain Set1 (AIA attributes)
 2. aia_shape_solarXY.mat and aia_shape_solarXY.csv contain Set2 (AIA and shape attributes)
 3. aia_hmi_magXY_cor.mat and aia_hmi_magXY_cor.csv contain Set 3 (AIA and HMI attributes)
 4. aia_hmi_shape_mag_solarXY_cor.mat and aia_hmi_shape_mag_solarXY_cor.csv contain Set 4(AIA,HMI, and shape attributes).

 * Folder ADASYN contains the program of Dominic Siedhoff available from
https://nl.mathworks.com/matlabcentral/fileexchange/50541-adasyn--improves-class-balance--extension-of-smote-
and included here for completeness. Please have a look at the ADASYN licence.

 * Folder ./results/ contains .mat files of results and .png graphics.
 