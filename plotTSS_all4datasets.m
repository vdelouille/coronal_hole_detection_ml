% plotTSS_all4datasets:
% Reproduce the plot of Figure 7 (Top) in 'Coronal Hole detection using supervised classification' by Delouille et al.
%
filename='resultsStr_uniform_aia_hmi_shape_mag_solarXY_cor.mat'

load(filename);
TSS1=TSS;

filename='resultsStr_uniform_aia_hmi_magXY_cor.mat'
load(filename);
TSS2=TSS;

filename='resultsStr_uniform_aia_shape_solarXY.mat'
load(filename);  
TSS3=TSS;

filename='resultsStr_uniform_aia_solarXY.mat'
load(filename);
TSS4=TSS;

pos1 = (0.8:1:4.8)';
pos2=(0.95:1:4.95)';
pos3=(1.1:1:5.1);
pos4=(1.3:1:5.3);

labels={'Linear SVM','SVM','Tree','RandomForest','kNN'};

figure(1)
h1=boxplot(TSS1,'Labels',labels,'PlotStyle', 'compact','LabelOrientation' ,'horizontal','Positions',pos1);
hold on
h2=boxplot(TSS2,'Labels',labels,'PlotStyle', 'compact','Colors', 'm' ,'LabelOrientation','horizontal','Positions',pos2);
hold on
h3=boxplot(TSS3,'Labels',labels,'PlotStyle', 'compact','Colors', 'g','LabelOrientation' ,'horizontal','Positions',pos3);
hold on
h4=boxplot(TSS4,'Labels',labels,'PlotStyle', 'compact','Colors', 'k','LabelOrientation' ,'horizontal','Positions',pos4);
ylabel('TSS ')
ylim([-0.1,1])
legend([h4(5,1),h3(5,1),h2(5,1),h1(5,1)], {'AIA', 'AIA, shape','AIA, HMI','AIA, shape, HMI'},'Location','southeast')

%legend('show')
ax = gca; % current axes
ax.FontSize = 12;
ax.TickDir = 'out';
ax.TickLength = [0.02 0.02];
print('TSS_four_datasets','-dpng')
hold off

