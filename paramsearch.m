function [TSS,FPR,TPR,Score,Ytrue,labels,Time_elapsed,beta_MdlSVC]= paramsearch(X,y,name,nrun,priorpdf)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  paramsearch.m : training and prediction for methods 'SVM Linear','SVM','Tree','RandomForest','kNN' on dataset X,y.
%                   This is repeated 'nrun' times. Results are stored in 'name'
% 
% [TSS,FPR,TPR,Score,Ytrue,NameLearner,Time_elapsed,beta_MdlSVC]= paramsearch(X,y,name,nrun,priorpdf)
%
% Input :   X : matrix n x p of feature vectors, where n is number of observations, p is number of features (attributes)
%	        y : vectors of labels (nx1) corresponding to features in X. 
%               y= 1 when features is a Filament Channel and y=0 for coronal holes)
%           name : string containing the name of a '.mat' file to store results (default is 'results.mat')
%           nrun : number of times the original dataset is split into training/test set and classification is done %                (default is nrun = 5)
%           priorpdf :prior distribution of classes. If set to 'empirical', nothing is done about class imbalance 
%               (equal misclassification cost are considered). If set to 'uniform', cost-sensitive learning is applied. 
%               (default is 'uniform')
%
% Output :  NameLearner: cell of size 1x nb_learners, containing string characters with the name of learners 
%                   Example: NameLearner={'Linear SVM','SVM','Tree','RandomForest','kNN'}; nb_learners = 5.
%           TSS : matrix of size nrun x nb_learners. 
%                 Element (i,j)  contains True Skill Statistics for run i and learner j 
%           FPR : matrix of size nrun x nb_learners. 
%           TPR : matrix of size nrun x nb_learners. 
%           Score : cell of size nrun x nb_learners.
%                   Cell(i,j) contains Score  of belonging to one class computed by Learner j  
%                    for observations in test  dataset in run i. 
%           Ytrue : cell of size nrun x nb_learners.
%                   Cell(i,j) contains true label for the test dataset in run i 
%           Time_elapsed : matrix of size nrun x nb_learners. 
%                          Element(i,j) contains the time needed in seconds to train classifier j during run i 
%           beta_MdlSVC : matrix of size nrun x p.
%                        Element (i,j) contains beta coefficient in the Linear SVM model for feature j in run i
%
% Requirement :  Matlab 2016b, Statistics and Machine Learning toolbox
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Set input parameter defaults
if nargin==4
    priorpdf='uniform';
elseif nargin ==3
    nrun = 5;
    priorpdf='uniform';
elseif nargin==2     
    name='results.mat';
    nrun=5;
    priorpdf='uniform';
end
%%% prepare vectors for outputs:
nb_learners = 5;

TSS = zeros(nrun,nb_learners);
FPR = zeros(nrun,nb_learners);
TPR = zeros(nrun,nb_learners);
Ytrue = cell(nrun,nb_learners);
Score=cell(nrun,nb_learners);
Time_elapsed = zeros(nrun,nb_learners);

[nobs, nfeatures]=size(X);
beta_MdlSVC = zeros(nrun,nfeatures);

labels={'Linear SVM','SVM','Tree','RandomForest','kNN'};

% Specify  hyperparameters
params = hyperparameters('fitclinear',X,y);
parametreSVM = params(1);

paramsSVMnonlin = hyperparameters('fitcsvm',X,y);
paramsSVMnonlin(3).Range = {'gaussian','polynomial'};

paramsRF = hyperparameters('fitcensemble',X,y,'Tree'); 
paramsRFchosen = paramsRF([2, 4:7]);


for k=1:nrun

    % Stratified shuffle-split for training/test set 
    % 'holdout' is the proportion of dataset kept for testing
    part = cvpartition(y,'holdout',0.25);
    istrain = training(part); % data for fitting
    istest = test(part);

    Xtr = X(istrain,:);
    ytr= y(istrain,:);
    
    Xtest = X(istest,:);
    ytrue= y(istest,:);

    % Stratified k-fold cross-validation for the choice of hyperparameters (k=5)
    cv_kfold = cvpartition(ytr,'KFold',5) ;

    %%% SVM Linear
    tic;
    MdlSVC = fitclinear(Xtr,ytr,'Learner','svm', ... 
      'prior',priorpdf, 'Verbose', 0 , 'Regularization','ridge',... 
      'OptimizeHyperparameters',parametreSVM,... 
      'HyperparameterOptimizationOptions',...
      struct('Optimizer','bayesopt','CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));
      toc;
    beta_MdlSVC(k,:)=MdlSVC.Beta;
    
    Time_elapsed(k,1)=toc;   

    [yfit,score] = predict(MdlSVC,Xtest);
    Score(k,1)={score(:,2)};
    Ytrue(k,1)={ytrue};

    TSS(k,1)=tss_def(ytrue,yfit);
    FPR(k,1)=fp_rate_def(ytrue,yfit);
    TPR(k,1)=tp_rate_def(ytrue,yfit);

    %% SVW with Gaussian and polynomial kernel
    tic;
    MdlSVM = fitcsvm(Xtr,ytr,'Verbose', 0, 'prior',priorpdf,'OptimizeHyperparameters',...
    paramsSVMnonlin ,'HyperparameterOptimizationOptions', ...
    struct('Optimizer','bayesopt', 'CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));
    Time_elapsed(k,2)=toc;    
    [yfit,score] = predict(MdlSVM,Xtest);
    Score(k,2)={score(:,2)};
    Ytrue(k,2)={ytrue};

    TSS(k,2)=tss_def(ytrue,yfit);
    FPR(k,2)=fp_rate_def(ytrue,yfit);
    TPR(k,2)=tp_rate_def(ytrue,yfit);

    % Fit Tree 
    tic;
    MdlTree = fitctree(Xtr,ytr, 'prior',priorpdf,'OptimizeHyperparameters','auto','HyperparameterOptimizationOptions',struct('Optimizer','bayesopt','CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));
    Time_elapsed(k,3)=toc;    
    [yfit,score] = predict(MdlTree,Xtest);
    Score(k,3)={score(:,2)};
    Ytrue(k,3)={ytrue};
    TSS(k,3)=tss_def(ytrue,yfit);
    FPR(k,3)=fp_rate_def(ytrue,yfit);
    TPR(k,3)=tp_rate_def(ytrue,yfit);


    % Random Forest (bagging of tree)
    tic;
    MdlRandomForest = fitcensemble(Xtr,ytr, 'prior',priorpdf,'Method','bag','OptimizeHyperparameters',paramsRFchosen,'HyperparameterOptimizationOptions',...
    struct('Optimizer','bayesopt','CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));
    Time_elapsed(k,4)=toc;    
    [yfit,score] = predict(MdlRandomForest,Xtest);
    Score(k,4)={score(:,2)};
    Ytrue(k,4)={ytrue};
    TSS(k,4)=tss_def(ytrue,yfit);
    FPR(k,4)=fp_rate_def(ytrue,yfit);
    TPR(k,4)=tp_rate_def(ytrue,yfit);

    % Fit k-nearest neighbor
    tic;
    MdlKNN = fitcknn(Xtr,ytr, 'prior',priorpdf,'OptimizeHyperparameters','auto','HyperparameterOptimizationOptions',struct('Optimizer','bayesopt','CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));
    Time_elapsed(k,5)=toc;    
    [yfit,score] = predict(MdlKNN,Xtest);
    Score(k,5)={score(:,2)};
    Ytrue(k,5)={ytrue};
    TSS(k,5)=tss_def(ytrue,yfit);
    FPR(k,5)=fp_rate_def(ytrue,yfit);
    TPR(k,5)=tp_rate_def(ytrue,yfit);

    save(name,'TSS','FPR','TPR','Score','Ytrue','labels','Time_elapsed','beta_MdlSVC');

    if mod(k,10)==0
        to_print = sprintf('Iterations %d',k);
        disp(to_print)
    end

end % end for nb-run

function tss=tss_def(y_true, y_pred)
    confmat = confusionmat(y_true, y_pred);
    tn = (confmat(1, 1));
    tp = (confmat(2, 2));
    fp = (confmat(1, 2));
    fn = (confmat(2, 1));
    tss=tp / (tp + fn) - fp / (fp + tn);

    
function tp_rate=tp_rate_def(y_true, y_pred)
    confmat = confusionmat(y_true, y_pred);
    tn = (confmat(1, 1));
    tp = (confmat(2, 2));
    fp = (confmat(1, 2));
    fn = (confmat(2, 1));
    tp_rate = tp / (fn + tp);
    
function fp_rate = fp_rate_def(y_true, y_pred)
    confmat = confusionmat(y_true, y_pred);
    tn = (confmat(1, 1));
    tp = (confmat(2, 2));
    fp = (confmat(1, 2));
    fn = (confmat(2, 1));
    fp_rate = fp / (fp + tn);
