function [X,y,header] = import_aia_hmi(filename, startRow, endRow)

%----------------------------------------------------------------------------------------------
% IMPORTFILE Import numeric data from a text file as column vectors.
%   [X,y,header]
%   = IMPORTFILE_AIA_HMI(FILENAME) Reads data from text file FILENAME for the
%   default selection.
%
%   [X,y,header]
%   = IMPORTFILE_AIA_HMI(FILENAME, STARTROW, ENDROW) Reads data from rows STARTROW
%   through ENDROW of text file FILENAME.
%
%   Here FILENAME must be 'aia_hmi_magXY_cor.csv' and outputs are:
%               X: a (nxp) matrix of feature vectors computed from AIA and HMI images,
%                    where n is number of observations, p is number of features
%               y: vectors of labels (nx1) corresponding to features in X. 
%                   y=1 when features is a Filament Channel and y=0 for coronal holes)
%               header: cell array of size px1, cell(i,1) contains the name of feature i
%
%
% Example:
%      [X,y,header]= import_aia_hmi('aia_hmi_magXY_cor.csv',2, 5004);
%
%    See also TEXTSCAN.
% X, y, header are then saved into 'aia_hmi_magXY_cor.mat'
%----------------------------------------------------------------------------------------------

%% Initialize variables.
delimiter = ',';
if nargin<=2
    startRow = 2;
    endRow = inf;
end

%% Format for each line of text:
%   column1: double (%f)
%	column2: double (%f)
%   column3: double (%f)
%	column4: double (%f)
%   column5: double (%f)
%	column6: double (%f)
%   column7: double (%f)
%	column8: double (%f)
%   column9: double (%f)
%	column10: double (%f)
%   column11: double (%f)
%	column12: double (%f)
%   column13: double (%f)
%	column14: double (%f)
%   column15: double (%f)
%	column16: double (%f)
%   column17: double (%f)
%	column18: double (%f)
%   column19: double (%f)
%	column20: double (%f)
%   column21: double (%f)
%	column22: double (%f)
%   column23: double (%f)
%	column24: double (%f)
%   column25: double (%f)
%	column26: double (%f)
%   column27: double (%f)
%	column28: double (%f)
%   column29: double (%f)
%	column30: double (%f)
%   column31: double (%f)
%	column32: double (%f)
%   column33: double (%f)
%	column34: double (%f)
%   column35: double (%f)
%	column36: double (%f)
%   column37: double (%f)
%	column38: double (%f)
%   column39: double (%f)
%	column40: double (%f)
%   column41: double (%f)
%	column42: double (%f)
%   column43: double (%f)
%	column44: double (%f)
%   column45: double (%f)
%	column46: double (%f)
%   column47: double (%f)
%	column48: double (%f)
%   column49: double (%f)
%	column50: double (%f)
%   column51: double (%f)
%	column52: double (%f)
%   column53: double (%f)
%	column54: double (%f)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', startRow(1)-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
for block=2:length(startRow)
    frewind(fileID);
    dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', startRow(block)-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
    for col=1:length(dataArray)
        dataArray{col} = [dataArray{col};dataArrayBlock{col}];
    end
end

%% Close the text file.
fclose(fileID);

%% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

%% Allocate imported array to column variable names
VarName1 = dataArray{:, 1};
absMeanAIA = dataArray{:, 2};
Contrast1AIA = dataArray{:, 3};
VarianceAIA = dataArray{:, 4};
Entropy1AIA = dataArray{:, 5};
Energy1AIA = dataArray{:, 6};
Energy2AIA = dataArray{:, 7};
Contrast2AIA = dataArray{:, 8};
CorrelationAIA = dataArray{:, 9};
SqVarianceAIA = dataArray{:, 10};
HomogeneityAIA = dataArray{:, 11};
SumAverageAIA = dataArray{:, 12};
SumVarianceAIA = dataArray{:, 13};
SumEntropyAIA = dataArray{:, 14};
EntropyAIA = dataArray{:, 15};
DiffVarianceAIA = dataArray{:, 16};
DiffEntropyAIA = dataArray{:, 17};
Info1AIA = dataArray{:, 18};
Info2AIA = dataArray{:, 19};
meanXAIA = dataArray{:, 20};
StdDevXAIA = dataArray{:, 21};
HXY1AIA = dataArray{:, 22};
HXY2AIA = dataArray{:, 23};
Areakm2 = dataArray{:, 24};
Areaarcsec2 = dataArray{:, 25};
Distancetodiskcenterarcsec = dataArray{:, 26};
minlat = dataArray{:, 27};
maxlat = dataArray{:, 28};
CofMlat = dataArray{:, 29};
CHFIL01 = dataArray{:, 30};
%
absMeanHMI = dataArray{:, 31};
Contrast1HMI = dataArray{:, 32};
VarianceHMI = dataArray{:, 33};
Entropy1HMI = dataArray{:, 34};
Energy1HMI = dataArray{:, 35};
PHIHMI = dataArray{:, 36};
Energy2HMI = dataArray{:, 37};
Contrast2HMI = dataArray{:, 38};
CorrelationHMI = dataArray{:, 39};
SqVarianceHMI = dataArray{:, 40};
HomogeneityHMI = dataArray{:, 41};
SumAverageHMI = dataArray{:, 42};
SumVarianceHMI = dataArray{:, 43};
SumEntropyHMI = dataArray{:, 44};
EntropyHMI = dataArray{:, 45};
DiffVarianceHMI = dataArray{:, 46};
DiffEntropyHMI = dataArray{:, 47};
Info1HMI = dataArray{:, 48};
Info2HMI = dataArray{:, 49};
meanXHMI = dataArray{:, 50};
StdDevXHMI = dataArray{:, 51};
HXY1HMI = dataArray{:, 52};
HXY2HMI = dataArray{:, 53};
MAGNETICFLUXHMI = dataArray{:, 54};


X=cat(2,absMeanAIA,Contrast1AIA,VarianceAIA,Entropy1AIA,Energy1AIA,Energy2AIA,Contrast2AIA,CorrelationAIA,...
SqVarianceAIA,HomogeneityAIA,SumAverageAIA,SumVarianceAIA,SumEntropyAIA,EntropyAIA,DiffVarianceAIA,DiffEntropyAIA,...
Info1AIA,Info2AIA,meanXAIA,StdDevXAIA,HXY1AIA,HXY2AIA,...
absMeanHMI,Contrast1HMI,VarianceHMI,Entropy1HMI,Energy1HMI,PHIHMI,Energy2HMI,Contrast2HMI,CorrelationHMI,SqVarianceHMI,...
HomogeneityHMI,SumAverageHMI,SumVarianceHMI,SumEntropyHMI,EntropyHMI,DiffVarianceHMI,DiffEntropyHMI,Info1HMI,Info2HMI,...
meanXHMI,StdDevXHMI,HXY1HMI,HXY2HMI,MAGNETICFLUXHMI,Areakm2,Distancetodiskcenterarcsec,minlat,maxlat,CofMlat);

y=CHFIL01;

%Keep the names of the variables in cell 'header':
header=cell(51,1);
header{1}=string('absMeanAIA');
header{2}=string('Contrast1AIA');
header{3}=string('VarianceAIA');
header{4}=string('Entropy1AIA');
header{5}=string('Energy1AIA');
header{6}=string('Energy2AIA');
header{7}=string('Contrast2AIA');
header{8}=string('CorrelationAIA');
header{9}=string('SqVarianceAIA');
header{10}=string('HomogeneityAIA');
header{11}=string('SumAverageAIA');
header{12}=string('SumVarianceAIA');
header{13}=string('SumEntropyAIA');
header{14}=string('EntropyAIA');
header{15}=string('DiffVarianceAIA');
header{16}=string('DiffEntropyAIA');
header{17}=string('Info1AIA');
header{18}=string('Info2AIA');
header{19}=string('meanXAIA');
header{20}=string('StdDevXAIA');
header{21}=string('HXY1AIA');
header{22}=string('HXY2AIA');
header{23}=string('absMeanHMI');
header{24}=string('Contrast1HMI');
header{25}=string('VarianceHMI');
header{26}=string('Entropy1HMI');
header{27}=string('Energy1HMI');
header{28}=string('PHIHMI');
header{29}=string('Energy2HMI');
header{30}=string('Contrast2HMI');
header{31}=string('CorrelationHMI');
header{32}=string('SqVarianceHMI');
header{33}=string('HomogeneityHMI');
header{34}=string('SumAverageHMI');
header{35}=string('SumVarianceHMI');
header{36}=string('SumEntropyHMI');
header{37}=string('EntropyHMI');
header{38}=string('DiffVarianceHMI');
header{39}=string('DiffEntropyHMI');
header{40}=string('Info1HMI');
header{41}=string('Info2HMI');
header{42}=string('meanXHMI');
header{43}=string('StdDevXHMI');
header{44}=string('HXY1HMI');
header{45}=string('HXY2HMI');
header{46}=string('MAGNETICFLUXHMI');
header{47}=string('Areakm2');
header{48}=string('Distancetodiskcenterarcsec');
header{49}=string('minlat'); 
header{50}=string('maxlat');
header{51}=string('CofMlat');





