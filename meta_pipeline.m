%
% meta_pipeline.m 
% This is the main function for performing all simulations used for the chapter 'Coronal Hole Detection using supervised 
% classification ' within the book on 'Machine Learning for Space Weather', Elsevier,  2017. 
% --------------------------------------------------------------
%
% You must first add the 'coronal_hole_detection_ml' folder and all its subfolders to your matlabpath 
% 
addpath(genpath('./coronal_hole_detection_ml/'))
pathdata='./coronal_hole_detection_ml/data/';

% You need to choose a dataset 'filename' (see below). It will contain:
%   header : cell of size 1 x p with string containing the names of the attributes (or features)
%           if filename = 'aia_solarXY.mat', the number of attributes p = 27.
%           if filename = 'aia_shape_solarXY.mat', p= 32.
%           if filename = 'aia_hmi_magXY_cor.mat', p = 51  
%           if filename = 'aia_hmi_shape_mag_solarXY_cor.mat', p= 56.
%   X : matrix n x p of feature vectors, where n is number of observations, p is number of attributes. We have n= 5003 observations.         
%   y : vectors of labels (nx1) corresponding to features in X. y= 1 when features is a Filament Channel and y=0 for Coronal Holes)

nrun =50;  % nrun = number of validation/test set split 


%1. Analyze  best scheme to handle imbalance:
% Use the whole set of attributes, featuring location, shape, AIA and HMI statistics
filename =strcat(pathdata,'aia_hmi_shape_mag_solarXY_cor.mat');

% No sampling techniques of original dataset, no cost-sensitive learning 
type_resampling = 'no_resampling';
priorpdf = 'empirical';
pipeline_paramsearch(filename,type_resampling,priorpdf,nrun);


%ADASYN upsampling without cost-sensitive learning.
type_resampling = 'adasyn';
priorpdf = 'empirical';
pipeline_paramsearch(filename,type_resampling,priorpdf,nrun);

% No sampling techniques, cost-sensitive learning 
type_resampling = 'no_resampling';
priorpdf = 'uniform';
pipeline_paramsearch(filename,type_resampling,priorpdf,nrun);

% ADASYN upsampling and cost-sensitive learning.
type_resampling = 'adasyn';
priorpdf = 'uniform';
pipeline_paramsearch(filename,type_resampling,priorpdf,nrun);


% Using ensemble method (ADABoost and RUSBoost)
type_resampling ='ensemble';
priorpdf = 'empirical';
pipeline_paramsearch(filename,type_resampling,priorpdf,nrun);

priorpdf = 'uniform';
pipeline_paramsearch(filename,type_resampling,priorpdf,nrun);


% 2. Study  the importance of attributes.
% With the scheme : type_resampling ='no_resampling' and priorpdf ='uniform', analyze the various input dataset:
type_resampling = 'no_resampling';
priorpdf = 'uniform';

% Using only location and AIA attributes
filename = strcat(pathdata,'aia_solarXY.mat');
pipeline_paramsearch(filename,type_resampling,priorpdf,nrun);

% Using location, shape, and AIA attributes
filename=strcat(pathdata,'aia_shape_solarXY.mat');
pipeline_paramsearch(filename,type_resampling,priorpdf,nrun);

% Using location AIA and HMI attributes
filename=strcat(pathdata,'aia_hmi_magXY_cor.mat');
pipeline_paramsearch(filename,type_resampling,priorpdf,nrun);

