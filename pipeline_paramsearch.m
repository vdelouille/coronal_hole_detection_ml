function pipeline_paramsearch(filename,type_resampling,priorpdf,nrun)
%---------------------------------------------------------------------
% pipeline_paramsearch.m is a Matlab script that: 
%             - load  labeled dataset 'filename' containing attributes and labels from class coronal holes (CH) and filament %                channel (FC),
%              - call functions to perform  supervised classification with Prior set to 'priorpdf' 
%                        and one of the three  following 'type_resampling':
%                        1. if 'type_resampling' = 'no_resampling': no resampling  is done and 'paramsearch.m' is called
%                        2. if 'type_resampling' = 'adasyn': oversampling of minority class is done
%                            (calls 'paramsearchADASYN.m')
%                        3. if 'type_resampling' = 'ensemble': an ensemble method  is used (calls 'paramsearch_ensemble.m')
%              - repeat the supervised classification 'nrun' times 
%              - save the results.
% CALL: pipeline_paramsearch(filename, type_resampling, priorpdf, nrun)
% Input:filename: must take one of the following values:
%           'aia_solarXY.mat'  : contains location and AIA attributes for FC and CH
%           'aia_shape_solarXY.mat'   : contains location, shape, and AIA attributes.
%           'aia_hmi_magXY_cor.mat' : contains location, AIA, and HMI attributes.
%           'aia_hmi_shape_mag_solarXY_cor.mat' : contains location, shape, AIA, and HMI 
%           attributes.
%       type_resampling: must take one of the three values:
%                       'no_resampling' : no resampling is done before calling the classifier
%                       'adasyn' : ADASYN upsampling is performed on the trained dataset
%                       'ensemble' : ensemble method  
%                  Default value is 'no_resampling' 
%       priorpdf : prior distribution of class samples.  priorpdf= 'empirical' means no 
%                  cost-sensitive learning is done; priorpdf='uniform' means 
%                  cost-sensitive learning  is performed
%                   Default value is 'uniform'
%       nrun : number of times the splitting of the dataset  into training/test set is 
%               performed
%               Default value is nrun = 5
%
% Output: For each type of pre-processing, the following is saved in a *.mat files:
%         * True Skill Statistics (TSS)  
%         * False Positive rate (FPR), 
%         * True Postitive rate (TPR), 
%         * Score  : result of classification
%         * Ytrue labels for the test set  (different for each run)
%         * Labels : string with names of classifiers 
%         In case of   ('type_resampling' = 'no_resampling' and 'type_resampling' = 'ensemble') :
%         * Time_elapsed :  computation needed time (in sec)   to train a classifier 
%         In case of ('type_resampling' = 'no_resampling'):
%         * beta_MdlSVC :  beta coefficient in the Linear SVM model for feature j in run i
%
%
% Example : pathdata = './data'
%           filename =strcat(pathdata,'aia_hmi_shape_mag_solarXY_cor.mat');
%           pipeline_paramsearch(filename,'no_resampling','uniform',50);
%

[pathstr,name,ext] = fileparts(filename) ;
load(filename);
% filename contains:
%   header : cell of size 1 x p with string containing the names of the attributes (or features)
%           if filename = 'aia_solarXY.mat', the number of attributes p = 27.
%           if finename = 'aia_shape_solarXY.mat', p= 32.
%           if finename = 'aia_hmi_shape_mag_solarXY_cor.mat', p= 56.
%   X : matrix n x p of feature vectors, where n is number of observations, p is number of attributes. We have n= 5003 observations.         
%   y : vectors of labels (nx1) corresponding to features in X. y= 1 when features is a Filament Channel and y=0 for Coronal Holes)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin==4
    nrun = 5;
elseif nargin ==3
    nrun = 5;
    priorpdf='uniform'
elseif nargin==2     
    nrun=5;
    priorpdf='uniform';
    type_resampling = 'no_resampling';
end




% Our goal is to compare performance of several classifiers, therefore the classification is repteated several times on different partitioning
% of the original set into training and test set.
% nrun= number of times the dataset is split into training and test sets 


switch type_resampling
    case 'no_resampling' 
        % Classification without initial resampling
        % 'Str' indicates that stratification is done for splitting and hyperparameterization 
        % optimization.
        name_res=strcat('resultsStr_',priorpdf,'_',name,'.mat');
        [TSS,FPR,TPR,Score,Ytrue,labels,Time_elapsed,beta_MdlSVC]= paramsearch(X,y,name_res,nrun,priorpdf);
        save(name_res,'TSS','FPR','TPR','Score','Ytrue','labels','Time_elapsed','beta_MdlSVC');
        var_to_clean={'TSS','FPR', 'TPR','Score', 'Ytrue','Time_elapsed','beta_MdlSVC'};
        clear(var_to_clean{:});
    case 'adasyn'
        %  Classification with oversampling of minority class using ADASYN
        name_res=strcat('resultsStr_ADASYN_',priorpdf,'_',name,'.mat');
        [TSS,FPR,TPR,Score,Ytrue,labels]= paramsearchADASYN(X,y,name_res,nrun,priorpdf);
        save(name_res,'TSS','FPR','TPR','Score','Ytrue','labels');
        var_to_clean={'TSS','FPR', 'TPR','Score', 'Ytrue'};
        clear(var_to_clean{:});
    case 'ensemble'
        %  Classification using ensemble methods (AdaBoost, RUSBoost)
        name_res=strcat('results_ensembleStr_',priorpdf,'_',name,'.mat');
        [TSS,FPR,TPR,Score,Ytrue,labels,Time_elapsed]= paramsearch_ensemble(X,y,name_res,nrun,priorpdf)
        save(name_res,'TSS','FPR','TPR','Score','Ytrue','labels','Time_elapsed');
        var_to_clean={'TSS','FPR', 'TPR','Score', 'Ytrue','Time_elapsed'};
        clear(var_to_clean{:});      
    otherwise
        warning('Unexpected type of resampling. No operation is performed.')
end












