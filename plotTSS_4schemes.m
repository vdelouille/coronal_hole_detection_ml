% plotTSS_4schemesmes:
% plot True Skill Statistics 
% Reproduce the plot of Fig4 and Fig6a in 'Coronal Hole detection using supervised classification' by Delouille et al.%
% 
filename='resultsStr_uniform_aia_hmi_shape_mag_solarXY_cor.mat'
load(filename);
TSS1=TSS;

filename='resultsStr_ADASYN_uniform_aia_hmi_shape_mag_solarXY_cor.mat'
load(filename);
TSS2=TSS;

filename='resultsStr_empirical_aia_hmi_shape_mag_solarXY_cor.mat'
load(filename);  
TSS3=TSS;

filename='resultsStr_ADASYN_empirical_aia_hmi_shape_mag_solarXY_cor.mat'
load(filename);
TSS4= TSS;

pos1 = (0.8:1:4.8)';
pos2=(0.95:1:4.95)';
pos3=(1.1:1:5.1);
pos4=(1.3:1:5.3);


labels={'Linear SVM','SVM','Tree','RandomForest','kNN'};

figure(1)
h1=boxplot(TSS1,'Labels',labels,'PlotStyle', 'compact','LabelOrientation' ,'horizontal','Positions',pos1);
hold on
h2=boxplot(TSS2,'Labels',labels,'PlotStyle', 'compact','Colors', 'm' ,'LabelOrientation','horizontal','Positions',pos2);
hold on
h3=boxplot(TSS3,'Labels',labels,'PlotStyle', 'compact','Colors', 'g','LabelOrientation' ,'horizontal','Positions',pos3);
hold on
h4=boxplot(TSS4,'Labels',labels,'PlotStyle', 'compact','Colors', 'k','LabelOrientation' ,'horizontal','Positions',pos4);
ylabel('TSS ')
ylim([-0.1,1])
legend([h3(5,1),h4(5,1),h1(5,1),h2(5,1)], {'No Prep, empirical prior', 'ADASYN, empirical prior','No Prep, uniform prior','ADASYN, uniform prior'},'Location','southeast')

%legend('show')
ax = gca; % current axes
ax.FontSize = 12;
ax.TickDir = 'out';
ax.TickLength = [0.02 0.02];
print('TSS_four_schemes','-dpng')
hold off
end


%-----------------------------------------------------------------
% With ADABoost and RUSBoost :


filename1='resultsStr_ensemble_empirical_aia_hmi_shape_mag_solarXY_cor.mat';
load(filename1);
TSS1=TSS;

filename2='resultsStr_ensemble_uniform_aia_hmi_shape_mag_solarXY_cor.mat';
load(filename2);
TSS2=TSS;

pos1 = (0.9:1:1.9)';
pos2=(1.1:1:2.1)';

figure(4)
h1=boxplot(TSS1,'Labels',labels,'PlotStyle', 'compact','LabelOrientation' ,'horizontal','Positions',pos1);
hold on
h2=boxplot(TSS2,'Labels',labels,'PlotStyle', 'compact', 'Colors', 'm','LabelOrientation','horizontal','Positions',pos2);

ylabel('TSS ')
ylim([-0.1,1])
legend([h1(5,1),h2(5,1)], {'Empirical prior','Uniform prior'},'Location','southeast')
ax = gca; % current axes
ax.FontSize = 12;
ax.TickDir = 'out';
ax.TickLength = [0.02 0.02];
print('TSS_Boosting_vsRUSBoost','-dpng')
