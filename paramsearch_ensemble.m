function [TSS,FPR,TPR,Score,Ytrue,labels]= paramsearch_ensemble(X,y,name,nrun,priorpdf)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  paramsearch_ensemble.m : training and prediction for ensemble methods 'ADABoost' and 'RUSBoost'  on dataset X,y.
%                   This is repeated 'nrun' times. Results are stored in 'name'
% [TSS,FPR,TPR,Score,Ytrue,NameLearner]= paramsearch_ensemble(X,y,name,nrun,priorpdf)
%
% Input :   X : matrix n x p of feature vectors, where n is number of observations, p is number of features (attributes)
%	        y : vectors of labels (nx1) corresponding to features in X. 
%               y= 1 when features is a Filament Channel and y=0 for coronal holes)
%           name : string containing the name of a '.mat' file to store results (default is 'results.mat')
%           nrun : number of times the original dataset is split into training/test set and classification is done %                (default is nrun = 5)
%           priorpdf :prior distribution of classes. If set to 'empirical', nothing is done about class imbalance 
%               (equal misclassification cost are considered). 
%               If set to 'uniform',cost-sensitive learning is applied.              
%               (default is 'uniform')
%
% Output :  NameLearner: cell of size 1x nb_learners, containing string characters with the name of learners 
%                   Example: NameLearner={'ADABoost','RUSBoost'}; nb_learners = 2.
%           TSS : matrix of size nrun x nb_learners. 
%                 Element (i,j)  contains True Skill Statistics for run i and learner j 
%           FPR : matrix of size nrun x nb_learners. 
%           TPR : matrix of size nrun x nb_learners. 
%           Score : cell of size nrun x nb_learners.
%                   Cell(i,j) contains Score  of belonging to one class computed by Learner j  
%                    for observations in test  dataset in run i. 
%           Ytrue : cell of size nrun x nb_learners.
%                   Cell(i,j) contains true label for the test dataset in run i 
%
%           Time_elapsed : matrix of size nrun x nb_learners. 
%                          Element(i,j) contains the time needed in seconds to train classifier j during run i 
%
% Requirement :  Matlab 2016b, Statistics and Machine Learning toolbox
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 
 
%%% Set default values of input parameters 
if nargin==4
    priorpdf='uniform';
elseif nargin ==3
    nrun = 5;
    priorpdf='uniform'
elseif nargin==2     
    name='solar.mat';
    nrun=5;
    priorpdf='uniform';
end

nb_learners = 2;
TSS = zeros(nrun,nb_learners);
FPR = zeros(nrun,nb_learners);
TPR = zeros(nrun,nb_learners);
Ytrue = cell(nrun,nb_learners);
Score=cell(nrun,nb_learners);
labels={'ADABoost','RUSBoost'};

% Set the hyperparameters
params = hyperparameters('fitcensemble',X,y,'Tree'); 
paramsENS = params(2:7);


for k=1:nrun
    %  Build training and test dataset 
    % 'holdout' = fraction of original dataset kept for testing
    part = cvpartition(y,'holdout',0.25);
    istrain = training(part); % data for fitting
    istest = test(part);

    Xtr = X(istrain,:);
    ytr= y(istrain,:);

    Xtest = X(istest,:);
    ytrue= y(istest,:);
    
    % Stratified k-fold cross-validation for the choice of hyperparameters (k=5)
    cv_kfold = cvpartition(ytr,'KFold',5) ;
    
    % AdaBoost.M1
    MdlADABoost = fitcensemble(Xtr,ytr,'prior',priorpdf,'Method','AdaBoostM1', 'OptimizeHyperparameters',paramsENS,'HyperparameterOptimizationOptions',...
     struct('Optimizer','bayesopt','CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));
    
    [yfit,score] = predict( MdlADABoost,Xtest);
    Score(k,1)={score(:,2)};
    Ytrue(k,1)={ytrue};
    TSS(k,1)=tss_def(ytrue,yfit);
    FPR(k,1)=fp_rate_def(ytrue,yfit);
    TPR(k,1)=tp_rate_def(ytrue,yfit);
       
    % RUSBoost:
    MdlRUSBoost = fitcensemble(Xtr,ytr,'prior',priorpdf,'Method','RUSBoost','RatioToSmallest', [5 1],...
    'OptimizeHyperparameters',paramsENS,'HyperparameterOptimizationOptions',...
      struct('Optimizer','bayesopt','CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));   
    [yfit,score] = predict(MdlRUSBoost,Xtest);
    Score(k,2)={score(:,2)};
    Ytrue(k,2)={ytrue};

    TSS(k,2)=tss_def(ytrue,yfit);
    FPR(k,2)=fp_rate_def(ytrue,yfit);
    TPR(k,2)=tp_rate_def(ytrue,yfit);
    
save(name,'TSS','FPR','TPR','Score','Ytrue','labels');   

if mod(k,10)==0
    to_print = sprintf('Iterations %d',k);
    disp(to_print)
end


end  % end for k=1:nrun

function tss=tss_def(y_true, y_pred)
    confmat = confusionmat(y_true, y_pred);
    tn = (confmat(1, 1));
    tp = (confmat(2, 2));
    fp = (confmat(1, 2));
    fn = (confmat(2, 1));
    tss=tp / (tp + fn) - fp / (fp + tn);
    
function tp_rate=tp_rate_def(y_true, y_pred)
    confmat = confusionmat(y_true, y_pred);
    tn = (confmat(1, 1));
    tp = (confmat(2, 2));
    fp = (confmat(1, 2));
    fn = (confmat(2, 1));
    tp_rate = tp / (fn + tp);
    
function fp_rate = fp_rate_def(y_true, y_pred)
    confmat = confusionmat(y_true, y_pred);
    tn = (confmat(1, 1));
    tp = (confmat(2, 2));
    fp = (confmat(1, 2));
    fn = (confmat(2, 1));
    fp_rate = fp / (fp + tn);
