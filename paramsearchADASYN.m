function [TSS,FPR,TPR,Score,Ytrue,labels]= paramsearchADASYN(X,y,name,nrun,priorpdf)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  paramsearchADASYN.m : training and prediction for methods 'SVM Linear','SVM','Tree','RandomForest','kNN' 
%                   on dataset X,y. The trained dataset is first upsampled with the ADASYN method.
%                   This is repeated 'nrun' times. Results are stored in 'name'
%
% [TSS,FPR,TPR,Score,Ytrue,NameLearner]= paramsearchADASYN(X,y,name,nrun,priorpdf)
%
% Input :   X : matrix n x p of feature vectors, where n is number of observations, p is number of features (attributes)
%	        y : vectors of labels (nx1) corresponding to features in X. 
%               y= 1 when features is a Filament Channel and y=0 for coronal holes)
%           name : string containing the name of a '.mat' file to store results (default is 'results.mat')
%           nrun : number of times the original dataset is split into training/test set and classification is done %                (default is nrun = 5)
%           priorpdf :prior distribution of classes. If set to 'empirical', nothing is done about class imbalance 
%               (equal misclassification cost are considered). If set to 'uniform', cost-sensitive learning is applied. 
%               In case where ADASYN resampling is performed, it is not advisable to put priorpdf to 'uniform'.
%               (default is 'empirical')
%
% Output :  NameLearner: cell of size 1x nb_learners, containing string characters with the name of learners 
%                   Example: NameLearner={'Linear SVM','SVM','Tree','RandomForest','kNN'}; nb_learners = 5.
%           TSS : matrix of size nrun x nb_learners. 
%                 Element (i,j)  contains True Skill Statistics for run i and learner j 
%           FPR : matrix of size nrun x nb_learners. 
%           TPR : matrix of size nrun x nb_learners. 
%           Score : cell of size nrun x nb_learners.
%                   Cell(i,j) contains Score  of belonging to one class computed by Learner j  
%                    for observations in test  dataset in run i. 
%           Ytrue : cell of size nrun x nb_learners.
%                   Cell(i,j) contains true label for the test dataset in run i 
%
% Requirement :  Matlab 2016b, Statistics and Machine Learning toolbox
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Set input parameter defaults
if nargin==4
    priorpdf='empirical';
elseif nargin ==3
    nrun = 5;
    priorpdf='empirical'
elseif nargin==2     
    name='results.mat';
    nrun=5;
    priorpdf='empirical';
end
%%% prepare vectors for outputs:
nb_learners = 5;

TSS = zeros(nrun,nb_learners);
FPR = zeros(nrun,nb_learners);
TPR = zeros(nrun,nb_learners);
Ytrue = cell(nrun,nb_learners);
Score=cell(nrun,nb_learners);

labels={'SVM Linear','SVM','Tree','RandomForest','kNN'};

% Specify  hyperparameters
params = hyperparameters('fitclinear',X,y);
parametreSVM = params(1);

paramsSVMnonlin = hyperparameters('fitcsvm',X,y);
paramsSVMnonlin(3).Range = {'gaussian','polynomial'};

paramsRF = hyperparameters('fitcensemble',X,y,'Tree'); 
paramsRFchosen = paramsRF([2, 4:7]);

for k=1:nrun

% Separate training and test dataset 
% holdout = partie pour le test dataset
part = cvpartition(y,'holdout',0.25);
istrain = training(part); % data for fitting
istest = test(part);

Xtr = X(istrain,:);
ytr= y(istrain,:);

Xtest = X(istest,:);
ytrue= y(istest,:);


% With oversampling of minority class using AdASYN in the training set only
maj= find(ytr==0); 
Xtr_maj= Xtr(maj,:);
ytr_maj = ytr(maj);
beta_improvement = 0.2

% 'beta_improvement = 0.2': achieve a ratio of 1:5 between minority and majority class

[Xtr_min, ytr_min] = ADASYN(Xtr,ytr,beta_improvement);
Xtr_OS=cat(1,Xtr_maj,Xtr_min);
ytr_OS=cat(1,ytr_maj,ytr_min);
var_to_clean={'Xtr_maj','Xtr_min', 'ytr_maj','ytr_min'};
clear(var_to_clean{:});

% Stratified k-fold cross-validation for the choice of hyperparameters (k=5) on upsampled dataset ytr_OS
cv_kfold = cvpartition(ytr_OS,'KFold',5) ;

%%% SVM Linear

MdlSVC = fitclinear(Xtr_OS,ytr_OS,'prior',priorpdf,'Learner','svm', ... 
      'prior',priorpdf, 'Verbose', 0 , 'Regularization','ridge',... 
      'OptimizeHyperparameters',parametreSVM,... 
      'HyperparameterOptimizationOptions',...
      struct('Optimizer','bayesopt','CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));
      
[yfit,score] = predict(MdlSVC,Xtest);
Score(k,1)={score(:,2)};
Ytrue(k,1)={ytrue};

TSS(k,1)=tss_def(ytrue,yfit);
FPR(k,1)=fp_rate_def(ytrue,yfit);
TPR(k,1)=tp_rate_def(ytrue,yfit);

% SVM Gaussian et polynomial 

MdlSVM = fitcsvm(Xtr_OS,ytr_OS,'prior',priorpdf,'Verbose', 0,'OptimizeHyperparameters',...
paramsSVMnonlin,'HyperparameterOptimizationOptions', ...
struct('Optimizer','bayesopt', 'CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));

[yfit,score] = predict(MdlSVM,Xtest);
Score(k,2)={score(:,2)};
Ytrue(k,2)={ytrue};

TSS(k,2)=tss_def(ytrue,yfit);
FPR(k,2)=fp_rate_def(ytrue,yfit);
TPR(k,2)=tp_rate_def(ytrue,yfit);

% Fit Tree 
MdlTree = fitctree(Xtr_OS,ytr_OS,'prior',priorpdf,'OptimizeHyperparameters','auto',...
'HyperparameterOptimizationOptions',struct('Optimizer','bayesopt','CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));
[yfit,score] = predict(MdlTree,Xtest);
Score(k,3)={score(:,2)};
Ytrue(k,3)={ytrue};
TSS(k,3)=tss_def(ytrue,yfit);
FPR(k,3)=fp_rate_def(ytrue,yfit);
TPR(k,3)=tp_rate_def(ytrue,yfit);


% Random Forest (bagging of tree)
MdlRandomForest = fitcensemble(Xtr_OS,ytr_OS,'prior',priorpdf,'Method','bag','OptimizeHyperparameters',paramsRFchosen,'HyperparameterOptimizationOptions',...
struct('Optimizer','bayesopt','CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));
 [yfit,score] = predict(MdlRandomForest,Xtest);
    Score(k,4)={score(:,2)};
    Ytrue(k,4)={ytrue};
    TSS(k,4)=tss_def(ytrue,yfit);
    FPR(k,4)=fp_rate_def(ytrue,yfit);
    TPR(k,4)=tp_rate_def(ytrue,yfit);


    % Fit k-nearest neighbor
MdlKNN = fitcknn(Xtr_OS,ytr_OS,'prior',priorpdf,'OptimizeHyperparameters','auto',...
 'HyperparameterOptimizationOptions',struct('Optimizer','bayesopt','CVPartition',cv_kfold,'Verbose',0,'ShowPlots',0));
[yfit,score] = predict(MdlKNN,Xtest);
Score(k,5)={score(:,2)};
Ytrue(k,5)={ytrue};
TSS(k,5)=tss_def(ytrue,yfit);
FPR(k,5)=fp_rate_def(ytrue,yfit);
TPR(k,5)=tp_rate_def(ytrue,yfit);

save(name,'TSS','FPR','TPR','Score','Ytrue','labels');

if mod(k,10)==0
    to_print = sprintf('Iterations %d',k);
    disp(to_print)
end

end % end for nb-run

function tss=tss_def(y_true, y_pred)
    confmat = confusionmat(y_true, y_pred);
    tn = (confmat(1, 1));
    tp = (confmat(2, 2));
    fp = (confmat(1, 2));
    fn = (confmat(2, 1));
    tss=tp / (tp + fn) - fp / (fp + tn);

    
function tp_rate=tp_rate_def(y_true, y_pred)
    confmat = confusionmat(y_true, y_pred);
    tn = (confmat(1, 1));
    tp = (confmat(2, 2));
    fp = (confmat(1, 2));
    fn = (confmat(2, 1));
    tp_rate = tp / (fn + tp);
    
function fp_rate = fp_rate_def(y_true, y_pred)
    confmat = confusionmat(y_true, y_pred);
    tn = (confmat(1, 1));
    tp = (confmat(2, 2));
    fp = (confmat(1, 2));
    fn = (confmat(2, 1));
    fp_rate = fp / (fp + tn);
